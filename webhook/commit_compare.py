"""Query MRs for upstream commit IDs to compare against submitted patches."""
import enum
import re
import sys

from cki_lib import logger
from cki_lib import misc
from cki_lib import session
# GitPython
from git import Repo

from . import cdlib
from . import common
from . import defs

LOGGER = logger.get_logger('cki.webhook.commit_compare')
SESSION = session.get_session('cki.webhook.commit_compare')


def extract_ucid(message):
    """Extract upstream commit ID from the message."""
    # pylint: disable=too-many-locals,too-many-branches
    message_lines = message.split('\n')
    gitref_list = []
    # Pattern for 'git show <commit>' (and git log) based backports
    gitlog_re = re.compile('^commit [a-f0-9]{40}$')
    # Pattern for 'git cherry-pick -x <commit>' based backports
    gitcherrypick_re = re.compile(r'^\(cherry picked from commit [a-f0-9]{40}\)$')
    # Bare hash matching pattern
    githash_re = re.compile('[a-f0-9]{40}', re.IGNORECASE)
    # Look for 'RHEL-only' patches
    rhelonly_re = re.compile('^Upstream( S|-s)tatus:.*RHEL.*only')
    # Look for 'Posted' patches, posted upstream but not in a git tree yet
    posted_re = re.compile('^Upstream( S|-s)tatus:.*Posted')
    # Look for 'Posted' patches, which are in a git tree we don't track (yet?)
    upstream_status_re = re.compile('^Upstream( S|-s)tatus: ')
    tree_prefixes = ('git://anongit.freedesktop.org/',
                     'https://anongit.freedesktop.org/git/',
                     'https://git.kernel.org/pub/scm/',
                     'git://git.kernel.org/pub/scm/',
                     'git://git.infradead.org/',
                     'http://git.linux-nfs.org/',
                     'git://linux-nfs.org/',
                     'https://github.com/',
                     'https://git.samba.org/')

    for line in message_lines:
        gitrefs = gitlog_re.findall(line)
        for ref in gitrefs:
            for githash in githash_re.findall(ref):
                if githash not in gitref_list:
                    gitref_list.append(githash)
        gitrefs = gitcherrypick_re.findall(line)
        for ref in gitrefs:
            for githash in githash_re.findall(ref):
                if githash not in gitref_list:
                    gitref_list.append(githash)
        if rhelonly_re.findall(line):
            gitref_list.append("RHELonly")
        elif posted_re.findall(line):
            gitref_list.append("Posted")
        elif upstream_status_re.findall(line):
            for prefix in tree_prefixes:
                if prefix in line:
                    LOGGER.info("Found tree_prefix in: %s", line)
                    gitref_list.append("Posted")
    # We return empty arrays if no commit IDs are found
    LOGGER.debug("Found upstream refs: %s", gitref_list)
    if len(gitref_list) > 5:
        LOGGER.info("Excessive number of commits found: %d, call it RHEL-only", len(gitref_list))
        gitref_list = ["RHELonly"]
    return gitref_list


def noucid_msg():
    """Return the message to use as a note for commits without an upstream commit ID."""
    msg = "No commit information found.  All commits are required to have a 'commit: "
    msg += "<upstream_commit_hash>' entry, or an 'Upstream Status: RHEL-only' entry "
    msg += "with an explanation why this change is not upstream. Alternatively, an "
    msg += "'Upstream Status: Posted' entry followed by a URL pointing to the posting "
    msg += "is also acceptable.  Please review this commit and appropriate metadata.  "
    msg += "Guidelines for these entries can be found in CommitRules,"
    msg += "https://red.ht/kwf_commit_rules."
    return msg


def posted_msg():
    """Return the message to use as note for commits that claim to be Posted."""
    msg = "This commit has Upstream Status as Posted, but we're not able to auto-compare it.  "
    msg += "Reviewers should take additional care when reviewing these commits."
    return msg


def merge_msg():
    """Return the message to use as note for commits that appear to be merge commits."""
    msg = "This commit looks like a merge commit, probably for dependencies in the series.  "
    msg += "Reviewers should evaluate these commits accordingly."
    return msg


def rhelonly_msg():
    """Return the message to use as note for commits that claim to be RHEL-only."""
    msg = "This commit has Upstream Status as RHEL-only and has no corresponding upstream commit.  "
    msg += "Reviewers should take additional care when reviewing these commits."
    return msg


def unk_cid_msg():
    """Return the message to use as note for commits that have an unknown commit ID."""
    msg = "No upstream source commit found.  This commit references an upstream commit ID, but "
    msg += "its source of origin is not recognized.  Please verify the upstream source tree."
    return msg


def diffs_msg():
    """Return the message to use as note for commits that differ from upstream."""
    msg = "This commit differs from the referenced upstream commit and should be evaluated "
    msg += "accordingly."
    return msg


def kabi_msg():
    """Return the message to use as note for commits that may contain kABI work-arounds."""
    msg = "This commit references a kABI work-around, and should be evaluated accordingly."
    return msg


def partial_msg():
    """Return the message to use as not for commits that match the part submitted."""
    msg = "This commit is a partial backport of the referenced upstream commit ID, and "
    msg += "the portions backported match upstream 100%."
    return msg


def badmail_msg():
    """Return the message to use for commits with invalid authorship."""
    msg = "This commit has invalid authorship. Commits must either have an authorship "
    msg += "email in the redhat.com domain, or they must match the email address of the "
    msg += "submitter of the merge request for outside contributors."
    return msg


class Match(enum.IntEnum):
    """Match versus upstream commit's diff."""

    NOUCID = 0
    FULL = 1
    PARTIAL = 2
    DIFFS = 3
    KABI = 4
    RHELONLY = 5
    POSTED = 6
    BADMAIL = 7
    MERGECOMMIT = 8
    SKIP = 9


def get_report_table(reviewed_items):
    """Create the table for the upstream commit ID report."""
    mr_approved = True
    table = []
    for ucid in reviewed_items:
        ucid_properties = reviewed_items[ucid]["validation"]
        mr_approved = mr_approved and ucid_properties['match'] != Match.NOUCID
        mr_approved = mr_approved and ucid_properties['match'] != Match.BADMAIL
        commits = []
        for commit in reviewed_items[ucid]["commits"]:
            commits.append(commit.id)
        table.append([ucid, commits, ucid_properties['match'],
                      ucid_properties['notes'], ucid_properties['logs']])
    return (table, mr_approved)


def get_match_info(match):
    """Get info on match type."""
    mstr = "No UCID   "
    full_match = False
    if match == Match.FULL:
        mstr = "100% match"
        full_match = True
    elif match == Match.PARTIAL:
        mstr = "Partial   "
    elif match == Match.DIFFS:
        mstr = "Diffs     "
    elif match == Match.KABI:
        mstr = "kABI Diffs"
    elif match == Match.RHELONLY:
        mstr = "n/a       "
    elif match == Match.POSTED:
        mstr = "n/a       "
    elif match == Match.BADMAIL:
        mstr = "Bad email "
    elif match == Match.MERGECOMMIT:
        mstr = "Merge commit"
    return (mstr, full_match)


def has_upstream_commit_hash(entry):
    """Figure out if this patch has an upstream commit hash or not."""
    if entry == Match.NOUCID:
        return False
    if entry == Match.RHELONLY:
        return False
    if entry == Match.POSTED:
        return False
    if entry == Match.MERGECOMMIT:
        return False
    return True


def print_gitlab_report(notes, table, mr_approved, count):
    """Print an upstream commit ID mapping report for gitlab."""
    kerneloscope = "http://kerneloscope.usersys.redhat.com"
    full_match = True
    show_full_match_note = False
    evaluated_commits = 0
    report = "<br>\n\n**Upstream Commit ID Readiness "
    if mr_approved:
        report += "Report**\n\n"
    else:
        report += "Error(s)!**\n\n"
    report += "This report indicates how backported commits compare to the upstream source " \
              "commit.  Matching (or not matching) is not a guarantee of correctness.  KABI, " \
              "missing or un-backportable dependencies, or existing RHEL differences against " \
              "upstream may lead to a difference in commits. As always, care should be taken " \
              "in the review to ensure code correctness.\n\n" \
              "|P num   |UCID    |Sub CID |Match     |Notes   |\n" \
              "|:-------|:-------|:-------|:---------|:-------|\n"
    for row in reversed(table):
        if row[2] == Match.FULL:
            show_full_match_note = True
            evaluated_commits += 1
            continue
        if row[2] == Match.MERGECOMMIT or row[2] == Match.SKIP:
            continue
        if row[0] != "-" and has_upstream_commit_hash(row[2]):
            ucidh = f"[{row[0][:8]}]({kerneloscope}/commit/{row[0]})"
        else:
            ucidh = f"{row[0][:8]}"
        commits = row[1] if len(row[1]) < 26 else row[1][:25] + ["(...)"]
        evaluated_commits += len(row[1])
        report += "|"+str(evaluated_commits)
        report += "|"+ucidh+"|"+"<br>".join(commits)
        (mstr, match_status) = get_match_info(row[2])
        full_match = full_match and match_status
        report += "|"+mstr+"|"
        report += common.build_note_string(row[3])

    report += f"\n\nTotal number of commits analyzed: **{evaluated_commits}**<br>"
    if count != evaluated_commits:
        report += f"* Skipped dependency commits: **{count - evaluated_commits}**<br>"
    if show_full_match_note:
        report += "* Patches that match upstream 100% not shown in table<br>"
    report += "\n"
    report += common.print_notes(notes)
    if mr_approved:
        report += "Merge Request upstream commit IDs all present.\n" \
                  "Please note and evaluate differences from upstream in the table.\n"
    else:
        report += "\nThis Merge Request contains commits that are missing upstream commit ID " \
                  "references. Please review the table. \n" \
                  " \n" \
                  "To request re-evalution after resolving any issues with the commits in the " \
                  "merge request, add a comment to this MR with only the text:  " \
                  "request-commit-id-evaluation \n"

    return (report, full_match)


def print_text_report(notes, table, mr_approved, count):
    """Print an upstream commit ID mapping report for the text console."""
    full_match = True
    show_full_match_note = False
    evaluated_commits = 0
    report = "\n\nUpstream Commit ID Readiness Report\n"
    report += "* Patches that match upstream 100% not shown\n\n"
    report += "|P num   |UCID    |Sub CID |Match     |Notes   |\n"
    report += "|:-------|:-------|:-------|:---------|:-------|\n"
    for row in reversed(table):
        if row[2] == Match.FULL:
            show_full_match_note = True
            evaluated_commits += 1
            continue
        if row[2] == Match.MERGECOMMIT or row[2] == Match.SKIP:
            continue
        commits = common.build_commits_for_row(row)
        evaluated_commits += len(row[1])
        report += "|"+str(evaluated_commits)
        report += "|"+str(row[0][:8])+"|"+" ".join(commits)
        (mstr, match_status) = get_match_info(row[2])
        full_match = full_match and match_status
        report += "|"+mstr+"|"+", ".join(row[3])+"|\n"
    report += common.print_notes(notes)
    status = "passes" if mr_approved else "fails"
    report += f"Merge Request {status} upstream commit ID validation.\n"
    if mr_approved:
        report += "Please verify differences from upstream.\n"
    report += f"Total number of commits analyzed: **{evaluated_commits}**\n"
    if count != evaluated_commits:
        report += f"* Skipped dependency commits: **{count - evaluated_commits}**\n"
    if show_full_match_note:
        report += "* Patches that match upstream 100% not shown in table\n"
    return (report, full_match)


def build_review_lists(commit, review_lists, ucid):
    """Build review_lists for this commit ID."""
    cid_data = review_lists[ucid] if ucid in review_lists else {}
    commits = cid_data["commits"] if "commits" in cid_data else []
    commits.append(commit)
    cid_data["commits"] = commits
    return cid_data


def get_upstream_diff(mri, ucid, filelist):
    """Extract diff for upstream commit ID, optionally limiting to filelist."""
    repo = Repo(mri.linux_src)

    try:
        commit = repo.commit(ucid)
    # pylint: disable=broad-except
    except Exception:
        LOGGER.debug("Commit ID %s not found in upstream", ucid)
        return None

    diff = repo.git.diff(f"{commit}^", commit)
    # author_name = commit.author.name
    author_email = commit.author.email

    if len(filelist) == 0:
        return (diff, author_email)

    LOGGER.debug("Getting partial diff for %s", ucid)
    part_diff = cdlib.get_partial_diff(diff, filelist)

    return (part_diff, author_email)


def find_kabi_hints(message, diff):
    """Check for hints this patch has kABI workarounds in it."""
    for keyword in ("genksyms", "kabi", "rh_reserved"):
        if keyword in message.lower():
            return True
        if keyword in diff.lower():
            return True
    return False


def process_no_ucid_commit(mri):
    """Process a commit w/no associated upstream commit ID."""
    mri.notes += [] if mri.noucid_id else [noucid_msg()]
    mri.noucid_id = mri.noucid_id if mri.noucid_id else str(len(mri.notes))
    ucid_properties = {'match': Match.NOUCID, 'notes': [], 'logs': ''}
    ucid_properties['notes'].append(mri.noucid_id)
    return ucid_properties


def process_rhel_only_commit(mri):
    """Process a commit listed as being RHEL-only."""
    mri.notes += [] if mri.rhelonly_id else [rhelonly_msg()]
    mri.rhelonly_id = mri.rhelonly_id if mri.rhelonly_id else str(len(mri.notes))
    ucid_properties = {'match': Match.RHELONLY, 'notes': [], 'logs': ''}
    ucid_properties['notes'].append(mri.rhelonly_id)
    return ucid_properties


def process_posted_commit(mri):
    """Process a commit listed as being Posted upstream."""
    mri.notes += [] if mri.posted_id else [posted_msg()]
    mri.posted_id = mri.posted_id if mri.posted_id else str(len(mri.notes))
    ucid_properties = {'match': Match.POSTED, 'notes': [], 'logs': ''}
    ucid_properties['notes'].append(mri.posted_id)
    return ucid_properties


def process_merge_commit(mri):
    """Process a commit that looks to be a merge commit (probably for dependencies)."""
    mri.notes += [] if mri.merge_id else [merge_msg()]
    mri.merge_id = mri.merge_id if mri.merge_id else str(len(mri.notes))
    ucid_properties = {'match': Match.MERGECOMMIT, 'notes': [], 'logs': ''}
    ucid_properties['notes'].append(mri.merge_id)
    return ucid_properties


def process_unknown_commit(mri):
    """Process a commit with an unrecognized commit ID ref."""
    mri.notes += [] if mri.unknown_id else [unk_cid_msg()]
    mri.unknown_id = mri.unknown_id if mri.unknown_id else str(len(mri.notes))
    ucid_properties = {'match': Match.NOUCID, 'notes': [], 'logs': ''}
    ucid_properties['notes'].append(mri.unknown_id)
    return ucid_properties


def skip_this_entry():
    """Process a review_list entry we already have a valid Posted/RHEL-only ref for."""
    ucid_properties = {'match': Match.SKIP, 'notes': [], 'logs': ''}
    return ucid_properties


def process_partial_backport(mri):
    """Process a commit that is a partial backport of an upstream commit."""
    mri.notes += [] if mri.part_id else [partial_msg()]
    mri.part_id = mri.part_id if mri.part_id else str(len(mri.notes))
    ucid_properties = {'match': Match.PARTIAL, 'notes': [], 'logs': ''}
    ucid_properties['notes'].append(mri.part_id)
    return ucid_properties


def process_kabi_patch(mri):
    """Process a commit that references kABI workarounds."""
    mri.notes += [] if mri.kabi_id else [kabi_msg()]
    mri.kabi_id = mri.kabi_id if mri.kabi_id else str(len(mri.notes))
    ucid_properties = {'match': Match.KABI, 'notes': [], 'logs': ''}
    ucid_properties['notes'].append(mri.kabi_id)
    return ucid_properties


def process_commit_with_diffs(mri):
    """Process a commit with differences from upstream."""
    mri.notes += [] if mri.diffs_id else [diffs_msg()]
    mri.diffs_id = mri.diffs_id if mri.diffs_id else str(len(mri.notes))
    ucid_properties = {'match': Match.DIFFS, 'notes': [], 'logs': ''}
    ucid_properties['notes'].append(mri.diffs_id)
    return ucid_properties


def process_invalid_author_commit(mri):
    """Process a commit with invalid authorship."""
    mri.notes += [] if mri.badmail_id else [badmail_msg()]
    mri.badmail_id = mri.badmail_id if mri.badmail_id else str(len(mri.notes))
    ucid_properties = {'match': Match.BADMAIL, 'notes': [], 'logs': ''}
    ucid_properties['notes'].append(mri.badmail_id)
    return ucid_properties


def valid_rhcommit_author(rhcommit_email, upstream_email, mr_email):
    """Attempt to make sure we have proper authorship on submitted commits."""
    # if the commit has a redhat.com author, it's valid
    rhcommit_email_parts = rhcommit_email.split('@')
    if len(rhcommit_email_parts) != 2:
        LOGGER.info("Committer email address (%s) not valid", rhcommit_email)
        return False

    if rhcommit_email_parts[1] == 'redhat.com':
        return True

    # if the commit author email matches the upstream email, but not the MR submitter email,
    # then they probably cherry-picked without a follow-up git commit -a --reset-author
    if rhcommit_email == upstream_email and rhcommit_email != mr_email:
        LOGGER.debug("Email on commit doesn't match MR submitter email")
        return False

    # if the commit author and MR submitter emails match, probably an outside contributor
    if rhcommit_email == mr_email:
        return True

    # anything else... let's flag it invalid.
    return False


def no_upstream_commit_data(mri, ucid):
    """Handle a commit that has no upstream commit ID data."""
    if ucid == "-":
        return process_no_ucid_commit(mri)

    if ucid == "RHELonly":
        return process_rhel_only_commit(mri)

    if ucid == "Posted":
        return process_posted_commit(mri)

    if ucid == "Merge":
        return process_merge_commit(mri)

    return None


def validate_commit_ids(mri, review_lists):
    """Iterate through the upstream commit IDs we found and compare w/our submitted commits."""
    # pylint: disable=too-many-branches
    idiff = ""
    rhcommit_email = ""
    valid_no_ucid = []

    for ucid in review_lists:
        commit_ids = review_lists[ucid]["commits"]
        if ucid in ("-", "RHELonly", "Posted", "Merge"):
            for commit in commit_ids:
                valid_no_ucid.append(commit.id)

    for ucid in review_lists:
        commit_ids = review_lists[ucid]["commits"]

        if ucid in ("-", "RHELonly"):
            for commit in commit_ids:
                review_lists[ucid]["validation"] = no_upstream_commit_data(mri, ucid)
            continue

        upstream = get_upstream_diff(mri, ucid, [])
        if upstream is None:
            for commit in commit_ids:
                if commit.id not in valid_no_ucid:
                    if ucid in ("Posted", "Merge"):
                        review_lists[ucid]["validation"] = no_upstream_commit_data(mri, ucid)
                    else:
                        review_lists[ucid]["validation"] = process_unknown_commit(mri)
                else:
                    review_lists[ucid]["validation"] = skip_this_entry()
            continue

        for commit in commit_ids:
            mydiff = cdlib.get_submitted_diff(commit.diff())
            rhcommit_email = commit.author_email
            idiff = cdlib.compare_commits(upstream[0], mydiff[0])

        if not valid_rhcommit_author(rhcommit_email, upstream[1], mri.submitter_email):
            review_lists[ucid]["validation"] = process_invalid_author_commit(mri)
            continue

        if len(idiff) == 0:
            review_lists[ucid]["validation"] = {'match': Match.FULL, 'notes': [], 'logs': ''}
        else:
            upstream_partial = get_upstream_diff(mri, ucid, mydiff[1])
            idiff = cdlib.compare_commits(upstream_partial[0], mydiff[0])
            if len(idiff) == 0:
                review_lists[ucid]["validation"] = process_partial_backport(mri)
            elif find_kabi_hints(commit.message, mydiff[0]):
                review_lists[ucid]["validation"] = process_kabi_patch(mri)
            else:
                review_lists[ucid]["validation"] = process_commit_with_diffs(mri)


def add_kabi_label(mri, table):
    """Add a kabi label if the MR containts a commit flagged as kabi-related."""
    for row in table:
        if row[2] == Match.KABI:
            common.add_label_to_merge_request(mri.lab, mri.project, mri.merge_request.iid, ['KABI'])
            return True
    return False


def check_on_ucids(mri):
    """Check upstream commit IDs."""
    review_lists = {}
    dep_label = cdlib.set_dependencies_label(mri.lab, mri.project, mri.merge_request)
    # re-fetch MR to get updated Dependencies label, if needed
    if dep_label not in mri.merge_request.labels:
        mri.merge_request = mri.project.mergerequests.get(mri.merge_request.iid)
    (has_deps, dep_sha) = cdlib.get_dependencies_data(mri.merge_request)
    count = len(list(mri.merge_request.commits()))
    for commit in mri.merge_request.commits():
        # Exit the loop once we hit the first dependency commit
        if has_deps and cdlib.is_first_dep(commit, dep_sha):
            break
        try:
            found_refs = extract_ucid(commit.message)
        # pylint: disable=broad-except
        except Exception:
            found_refs = []

        for ucid in found_refs:
            review_lists[ucid] = build_review_lists(commit, review_lists, ucid)

        if not found_refs:
            review_lists["-"] = build_review_lists(commit, review_lists, "-")

    validate_commit_ids(mri, review_lists)
    (table, approved) = get_report_table(review_lists)
    return (table, approved, count)


# pylint: disable=too-many-instance-attributes,too-few-public-methods
class MRUCIDInstance:
    """Merge request Instance."""

    def __init__(self, lab, project, merge_request, payload):
        """Initialize the commit ID comparison instance."""
        self.linux_src = "/usr/src/linux"
        self.lab = lab
        self.log_ok_scope = False
        self.project = project
        self.merge_request = merge_request
        self.submitter_email = ""
        self.payload = payload
        self.notes = []
        self.noucid_id = ""
        self.rhelonly_id = ""
        self.posted_id = ""
        self.merge_id = ""
        self.unknown_id = ""
        self.diffs_id = ""
        self.kabi_id = ""
        self.part_id = ""
        self.badmail_id = ""

    def run_ucid_validation(self):
        """Do the thing, check submitted patches vs. referenced upstream commit IDs."""
        if misc.is_production() and common.mr_is_closed(self.merge_request):
            LOGGER.info("Not running validation on closed MR %s", self.merge_request.iid)
            return

        LOGGER.info("Running upstream commit ID validation on MR %s", self.merge_request.iid)
        LOGGER.debug("Merge request description:\n%s", self.merge_request.description)
        (table, approved, count) = check_on_ucids(self)
        # append quick label "Commitrefs" with scope "OK" or "NeedsReview"
        status = defs.READY_SUFFIX if approved else defs.NEEDS_REVIEW_SUFFIX
        common.add_label_to_merge_request(self.lab, self.project, self.merge_request.iid,
                                          [f'CommitRefs::{status}'])
        # append quick label "KABI" if we flagged any commit as possibly impacting kabi
        if add_kabi_label(self, table):
            LOGGER.debug("This MR (%s) impacts kABI", self.merge_request.iid)
        if misc.is_production():
            (report, full_match) = print_gitlab_report(self.notes, table, approved, count)
            if not full_match:
                self.merge_request.notes.create({'body': report})
        else:
            LOGGER.info('Skipping adding report in non-production')
            (report, full_match) = print_text_report(self.notes, table, approved, count)
            if not full_match:
                LOGGER.info(report)
            else:
                LOGGER.info('Patches all match upstream 100%')
        if not approved:
            LOGGER.info("Some commits in MR %s require further manual inspection!",
                        self.merge_request.iid)


def get_mri(gl_instance, message, key):
    """Return a merge request instance for the webhook payload."""
    gl_project = gl_instance.projects.get(message.payload["project"]["id"])
    if not (gl_mergerequest := common.get_mr(gl_project, message.payload[key]["iid"])):
        return None
    return MRUCIDInstance(gl_instance, gl_project, gl_mergerequest, message.payload)


def perform_mri_tasks(mri, linux_src, label_changed):
    """Perform tasks if mri is valid."""
    if mri:
        mri.submitter_email = str(mri.lab.users.get(mri.merge_request.author['id']).public_email)
        mri.linux_src = linux_src
        mri.log_ok_scope = label_changed
        mri.run_ucid_validation()


def process_mr(gl_instance, message, linux_src, **_):
    """Process a merge request message."""
    label_changed = common.has_label_changed(message.payload, 'CommitRefs::',
                                             common.LabelPart.PREFIX)
    if not common.mr_action_affects_commits(message) and not label_changed:
        return

    mri = get_mri(gl_instance, message, "object_attributes")
    perform_mri_tasks(mri, linux_src, label_changed)


def process_note(gl_instance, message, linux_src, **_):
    """Process a note message."""
    LOGGER.debug("Checking note request\n")
    if "merge_request" not in message.payload:
        return

    mri = get_mri(gl_instance, message, "merge_request")
    if not common.force_webhook_evaluation(message.payload['object_attributes']['note'],
                                           'commit-id'):
        return

    perform_mri_tasks(mri, linux_src, True)


WEBHOOKS = {
    'merge_request': process_mr,
    'note': process_note,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('COMMIT_COMPARE')
    parser.add_argument('--linux-src',  **common.get_argparse_environ_opts('LINUX_SRC'),
                        help='Directory containing upstream Linux kernel git tree')
    args = parser.parse_args(args)
    if not args.linux_src:
        LOGGER.warning("No Linux source tree directory specified, using default")
    common.generic_loop(args, WEBHOOKS, linux_src=args.linux_src)


if __name__ == "__main__":
    main(sys.argv[1:])
