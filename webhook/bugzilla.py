"""Query bugzilla for MRs."""
import enum
import json
import re
import sys

from cki_lib import logger
from cki_lib import misc
from cki_lib import session
from gitlab.exceptions import GitlabGetError

from . import cdlib
from . import common
from . import defs

LOGGER = logger.get_logger('cki.webhook.bugzilla')


def get_project_file(project, branch, path, raw=True):
    """Return the given file from the project."""
    try:
        if raw:
            return project.files.raw(file_path=path, ref=branch)
        return project.files.get(file_path=path, ref=branch)
    except GitlabGetError:
        LOGGER.info('Failed to fetch. project: %s, branch: %s, path: %s, raw enabled: %s.',
                    project.name, branch, path, raw)
    return None


def get_project_major_ver(project):
    """Return the major version of the given project."""
    # All current rhel src kernel projects are named 'rhel-<major>'.
    major_ver = re.match(r'^(rhel-(alt-)?|centos-stream-)(?P<version>\d+)', project.name)
    if major_ver:
        return major_ver['version']
    LOGGER.debug("Unable to determine major version of project '%s' from its name.", project.name)

    # Try to get RHEL_MAJOR from the project's 'main' repo Makefiles. Barf.
    # In older releases Makefile.rhelver doesn't exist.
    makefile = get_project_file(project, 'main', 'Makefile.rhelver')
    if not makefile:
        makefile = get_project_file(project, 'main', 'Makefile')

    if not makefile:
        return None

    for line in makefile.splitlines():
        if line.decode().startswith('RHEL_MAJOR = '):
            return line.decode().split(' = ')[1].strip()

    LOGGER.error('Cannot find RHEL_MAJOR in Makefile.')
    return None


def generate_refname(major, minor, alt, suffix=''):
    """Generate the refname for gitbz."""
    if int(major) < 8:
        return f"rhel{alt}{major}.{minor}{suffix}"
    return f"rhel-{major}.{minor}.0{suffix}"


def get_mr_target_release(project, mr_target):
    """Determine the target release for the given MR."""
    major_ver = get_project_major_ver(project)
    if not major_ver:
        return None

    if project.name.startswith('centos-stream-'):
        return f'c{major_ver}s'

    alt = '-alt-' if '-alt-' in project.name else '-'

    # If the target branch of the MR is not 'main' then the MR is pointing
    # at a z-stream branch or an early RHEL major release (Alpha or Beta)
    if mr_target not in ('main', 'main-rt'):
        target_re = fr"^{major_ver}\.(\d+)(-([a-z]+)?)?(-rt)?$"
        regex = re.compile(target_re)
        match = regex.fullmatch(mr_target)
        if not match:
            return None
        minor_ver = match.group(1)
        suffix = "" if match.group(3) != "alpha" or minor_ver != "0" else "-alpha"
        suffix = suffix if match.group(3) != "beta" or minor_ver != "0" else "-beta"
        return generate_refname(major_ver, minor_ver, alt, suffix)

    # If the target branch is 'main' then we're aiming at the latest release.
    branch_search = f"^{major_ver}."
    branches = project.branches.list(search=branch_search)
    minor_ver = -1
    branch_regex = fr"^{major_ver}\.(\d+)$"
    regex = re.compile(branch_regex)
    for branch in branches:
        match = regex.fullmatch(branch.name)
        if match:
            branch_minor = int(match.group(1))
            minor_ver = max(minor_ver, branch_minor)
    minor_ver += 1
    return generate_refname(major_ver, minor_ver, alt)


def nobug_msg():
    """Return the message to use as a note for commits without bz."""
    msg = "No 'Bugzilla:' entry was found in these commits.  This project requires that each "
    msg += "commit have at least one 'Bugzilla:' entry.  Please add a 'Bugzilla: <bugzilla_URL>' "
    msg += "entry for each bugzilla.  Guidelines for 'Bugzilla:' entries can be found in "
    msg += "CommitRules, https://red.ht/kwf_commit_rules."
    return msg


def depbug_msg():
    """Return the message to use as note for commits that are dependencies."""
    msg = "These commits are for dependencies listed in the MR description.  Your MR will not be "
    msg += "merged until these dependencies are in main."
    return msg


def nomrbug_msg():
    """Return the message to note an unknown bug found in patches."""
    msg = "These commits contain a bugzilla number that was not listed in the merge request "
    msg += "description.  Please verify the bugzilla's URL is correct, or, add them to your MR "
    msg += "description as either a 'Bugzilla:' or a 'Depends:' entry.  Guidelines for these "
    msg += "entries can be found in CommitRules, https://red.ht/kwf_commit_rules."
    return msg


def notapproved_msg():
    """Return the message for bugs which are not approved."""
    msg = "The bugzilla associated with these commits is not approved at this time."
    return msg


def badinternalbug_msg():
    """Return the message that 'INTERNAL' bug touches wrong source files."""
    msg = "These commits are marked as 'INTERNAL' but were found to touch "
    msg += "source files outside of the redhat/ directory. 'INTERNAL' bugs "
    msg += "are only to be used for changes to files in the redhat/ directory."
    return msg


def validate_internal_bz(commit_list):
    """Check file list of 'INTERNAL' bug only touch files under redhat/ or .gitlab*."""
    allowed_paths = ['redhat/', '.gitlab']
    for commit in commit_list.values():
        for path in commit:
            if not path.startswith(tuple(allowed_paths)):
                return False
    return True


BUG_FIELDS = ['cf_verified',
              'component',
              'id',
              'keywords',
              'product',
              'status'
              ]


def bug_is_verified(bug_no, bzcon):
    """Confirm the bug is Verified."""
    bug = bzcon.getbug(bug_no, include_fields=BUG_FIELDS)
    if not bug or bug.id != int(bug_no):
        LOGGER.error("getbug() did not return a useful result for BZ %s.", bug_no)
        return False

    if bug.product.startswith('Red Hat Enterprise Linux') \
            and bug.component in {'kernel', 'kernel-rt'} \
            and 'Tested' in bug.cf_verified:
        LOGGER.debug('Bug %s is Verified.', bug.id)
        return True
    LOGGER.debug('Bug %s is not Verified.', bug.id)
    return False


def temporary_xmlrpc_fault(result):
    """Return True if the 'Invalid' gitbz result seems to be temporary."""
    regex = r'error: XMLRPC Fault #(?P<fault_code>\d*).*: (?P<fault_msg>.*)\)'
    match = re.search(regex, result)
    if not match:
        return True

    is_temporary = True
    code = int(match.group('fault_code'))
    msg = match.group('fault_msg')
    # 1XX codes seem to be permanent failures due to bad input (ex 101 is an unknown bug id).
    if 100 <= code < 200:
        is_temporary = False
    LOGGER.warning('gitbz-query returned bugzilla XMLRPC Fault #%s: %s', code, msg)
    return is_temporary


def validate_gitbz_response(response, *args, **kwargs):
    # pylint: disable=unused-argument
    """Validate the gitbz response was not a backend error."""
    # If the response is an error pass it through.
    if response.status_code != 200:
        return response

    response_json = response.json()
    print(json.dumps(response_json, indent=2))

    # Store the response result.
    response.result = response_json['result']

    # A 'result' of 'fail' does not distinguish between a bug which
    # is considered not ready and a backend failure which prevented
    # evaluation.
    if 'Invalid' in response_json['logs']:
        # Certain XMLRPC errors should not trigger raise_for_status().
        if temporary_xmlrpc_fault(response_json['logs']):
            response.status_code = 502
    return response


def gitbz_query(gitbz_session, input_json):
    """Set up a session as needed and return whether the evaluation passed."""
    if gitbz_session is None:
        gitbz_url = 'https://dist-git.host.prod.eng.bos.redhat.com/lookaside/gitbz-query.cgi'
        retry_args = {'status_forcelist': [500, 502, 503, 504]}
        gitbz_session = session.get_session('cki.webhook.bugzilla', retry_args=retry_args)
        gitbz_session.allowed_methods = ['POST']
        gitbz_session.hooks['response'].append(validate_gitbz_response)

    print(json.dumps(input_json, indent=2))
    response = gitbz_session.post(gitbz_url, json=input_json)

    # Raise an error if we didn't get a nice response from gitbz-query.
    response.raise_for_status()

    if response.result == 'ok':
        return True
    return False


class BZState(enum.IntEnum):
    """Relevant BZ states for an MR."""

    NOT_READY = 0
    READY_FOR_QA = 1
    READY_FOR_MERGE = 2


def validate_bzs(project, merge_request, bugzilla_ids, bz_dependencies, reviewed_items, bzcon):
    # pylint: disable=too-many-locals,too-many-branches,too-many-statements,too-many-arguments
    """Validate bugzilla references."""
    gitbz_session = None
    target_branch = get_mr_target_release(project, merge_request.target_branch)
    suffix = '-alt' if '-alt-' in project.name else ''
    suffix = '-rt' if merge_request.target_branch.endswith('-rt') else suffix
    notes = []
    nobug_id = ""
    depbug_id = ""
    nomrbug_id = ""
    notgt_id = ""
    internalbug_id = ""
    for bug in reviewed_items:
        bz_properties = {'approved': BZState.NOT_READY, 'notes': [], 'logs': ''}
        reviewed_items[bug]["validation"] = bz_properties
        valid_mr_bug = True

        if not target_branch:
            notes += [] if notgt_id else ["Unknown repository target"]
            notgt_id = notgt_id if notgt_id else str(len(notes))
            bz_properties['notes'].append(notgt_id)
            continue

        if bug == 'INTERNAL':
            if validate_internal_bz(reviewed_items[bug]['commits']):
                bz_properties['approved'] = BZState.READY_FOR_MERGE
            else:
                notes += [] if internalbug_id else [badinternalbug_msg()]
                internalbug_id = internalbug_id if internalbug_id else str(len(notes))
                bz_properties['notes'].append(internalbug_id)
            continue

        if bug == "-":
            notes += [] if nobug_id else [nobug_msg()]
            nobug_id = nobug_id if nobug_id else str(len(notes))
            bz_properties['notes'].append(nobug_id)
            continue

        if bug in bz_dependencies:
            notes += [] if depbug_id else [depbug_msg()]
            depbug_id = depbug_id if depbug_id else str(len(notes))
            bz_properties['notes'].append(depbug_id)

        elif bug not in bugzilla_ids:
            notes += [] if nomrbug_id else [nomrbug_msg()]
            nomrbug_id = nomrbug_id if nomrbug_id else str(len(notes))
            bz_properties['notes'].append(nomrbug_id)
            valid_mr_bug = False

        buginputs = {
            'package': f'kernel{suffix}',
            'namespace': 'rpms',
            'ref': 'refs/heads/' + target_branch,
            'commits': [{
                'hexsha': 'HEAD',
                'files': ["kernel.spec"],
                'resolved': [int(bug)],
                'related': [],
                'reverted': [],
            }]
        }

        gitbz_status = gitbz_query(gitbz_session, buginputs)

        if valid_mr_bug and gitbz_status:
            if bug_is_verified(bug, bzcon):
                bz_properties['approved'] = BZState.READY_FOR_MERGE
            else:
                bz_properties['approved'] = BZState.READY_FOR_QA
        else:
            bz_properties['approved'] = BZState.NOT_READY
            noteid = 0
            while noteid < len(notes):
                if notes[noteid] == notapproved_msg():
                    break
                noteid += 1
            if noteid == len(notes):
                notes.append(notapproved_msg())
            bz_properties['notes'].append(str(noteid+1))
    return (notes, target_branch)


def get_report_table(reviewed_items):
    """Create the table for the bugzilla report."""
    mr_approved = BZState.READY_FOR_MERGE
    table = []
    for bug in reviewed_items:
        bz_properties = reviewed_items[bug]["validation"]
        mr_approved = min(mr_approved, bz_properties['approved'])
        commits = []
        for commit in reviewed_items[bug]["commits"]:
            commits.append(commit)
        table.append([bug, commits, bz_properties['approved'].name,
                      bz_properties['notes'], bz_properties['logs']])
    return (table, mr_approved)


def trim_notes(notes):
    """Trim/remove unwanted notes returned from dist-git cgi script."""
    noteid = 0
    while noteid < len(notes):
        trimmed = ""
        if notes[noteid] is None:
            noteid += 1
            continue
        strings = notes[noteid].split('\n')
        for string in strings:
            drop = False
            drop = string.startswith("*** eg: Resolves: rhbz#") or drop
            drop = string.startswith("*** Make sure to follow the") or drop
            drop = string.startswith("*** No issue IDs referenced") or drop
            drop = string.startswith("*** No approved issue") or drop
            drop = string.startswith("*** Unapproved issue") or drop
            drop = string.startswith("*** Commit") or drop
            trimmed += "" if drop else string + '\n'
        notes[noteid] = trimmed
        noteid += 1


def print_target(merge_request, bz_target):
    """Print the bugzilla target if MR targets main branch."""
    target = merge_request.target_branch
    target = target if target not in ("main", "main-rt") else f"{target} ({bz_target})"
    return "Target Branch: " + target


def print_gitlab_report(merge_request, notes, bz_target, table, mr_approved):
    """Print a bugzilla report for gitlab."""
    report = "<br>\n\n**Bugzilla Readiness "
    has_bugs = len(table) > 0
    desc_has_bugs_not_in_commits = False
    for row in table:
        if defs.BZ_IN_DESCRIPTION_ONLY in row[1]:
            desc_has_bugs_not_in_commits = True
    if mr_approved and has_bugs and not desc_has_bugs_not_in_commits:
        report += "Report**\n\n"
    else:
        report += "Error(s)!**\n\n"
    report += print_target(merge_request, bz_target) + "   \n"
    report += " \n\n"
    report += "|BZ|Commits|Approved|Notes|\n"
    report += "|:------|:------|:------|:-------|\n"
    for row in table:
        bzn = f"BZ-{row[0]}" if row[0] != "-" else row[0]
        commits = row[1] if len(row[1]) < 26 else row[1][:25] + ["(...)"]
        report += "|"+bzn+"|"+"<br>".join(commits)
        report += "|"+str(row[2])+"|"
        report += common.build_note_string(row[3])

    report += "\n\n"
    report += common.print_notes(notes)
    if mr_approved and has_bugs:
        report += "Merge Request passes bugzilla validation\n"
    else:
        report += "\nMerge Request fails bugzilla validation.  \n"
        report += " \n"
        report += "To request re-evalution after getting bugzilla approval "
        report += "add a comment to this MR with only the text: "
        report += "request-bz-evaluation  \n"
    if not has_bugs:
        report += "There were no valid bugs in the MR description or in any commits\n"
    if desc_has_bugs_not_in_commits:
        report += "There are bugs in the MR description not in any commits\n"

    return report


def print_text_report(merge_request, notes, bz_target, table, mr_approved):
    """Print a bugzilla report for the text console."""
    report = "\n\nBZ Readiness Report\n\n"
    has_bugs = len(table) > 0
    desc_has_bugs_not_in_commits = False
    report += print_target(merge_request, bz_target) + "\n\n"
    for row in table:
        if defs.BZ_IN_DESCRIPTION_ONLY in row[1]:
            desc_has_bugs_not_in_commits = True
        commits = common.build_commits_for_row(row)
        report += "|"+str(row[0])+"|"+" ".join(commits)
        report += "|"+str(row[2])+"|"+", ".join(row[3])+"|\n\n"
    report += common.print_notes(notes)
    status = "passes" if mr_approved and has_bugs else "fails"
    report += f"Merge Request {status} bugzilla validation\n"
    if not has_bugs:
        report += "There were no valid bugs in the MR description or in any commits\n"
    if desc_has_bugs_not_in_commits:
        report += "There are bugs in the MR description not in any commits\n"
    return report


def build_review_lists(commit, review_lists, bug):
    """Build review_lists for this bug."""
    found_files = []
    if bug == 'INTERNAL' and commit is not None:
        found_files = cdlib.extract_files(commit)

    bugd = review_lists[bug] if bug in review_lists else {}
    commits = bugd["commits"] if "commits" in bugd else {}
    if commit is not None:
        commits[commit.id] = found_files
    else:
        commits[defs.BZ_IN_DESCRIPTION_ONLY] = found_files
    bugd["commits"] = commits
    return bugd


def check_on_bzs(project, merge_request, bugzilla_ids, bz_dependencies, bzcon):
    # pylint: disable=too-many-locals
    """Check BZs."""
    review_lists = {}
    for commit in merge_request.commits():
        commit = project.commits.get(commit.id)
        # Skip merge commits, bugzilla link on the commit log for them
        # is not required and we do not care if bugs are listed on them
        if len(commit.parent_ids) > 1:
            continue
        try:
            found_bzs, non_mr_bzs, dep_bzs = common.extract_all_bzs(commit.message,
                                                                    bugzilla_ids,
                                                                    bz_dependencies)
        # pylint: disable=broad-except
        except Exception:
            found_bzs = []
            non_mr_bzs = []

        for bug in found_bzs:
            review_lists[bug] = build_review_lists(commit, review_lists, bug)

        for bug in non_mr_bzs:
            review_lists[bug] = build_review_lists(commit, review_lists, bug)

        for bug in dep_bzs:
            review_lists[bug] = build_review_lists(commit, review_lists, bug)

        if not found_bzs and not non_mr_bzs and not dep_bzs:
            review_lists["-"] = build_review_lists(commit, review_lists, "-")

    if len(review_lists) == 0:
        for bug in bugzilla_ids:
            review_lists[bug] = build_review_lists(None, review_lists, bug)
    (notes, tgt) = validate_bzs(project, merge_request, bugzilla_ids, bz_dependencies,
                                review_lists, bzcon)
    (table, approved) = get_report_table(review_lists)
    trim_notes(notes)

    return (notes, tgt, table, approved)


def set_bugs_to_modified(project, mrid, bug_list, bzcon):
    """Set relevant bugzillas to MODIFIED."""
    update_bug_list = []

    # Re-read merge request data to get latest labels, which may have been recently updated
    merge_request = project.mergerequests.get(mrid)
    ready_labels = [defs.READY_FOR_QA_LABEL, defs.READY_FOR_MERGE_LABEL]
    if not any(label in merge_request.labels for label in ready_labels):
        LOGGER.debug("This merge request has no %s or %s label, not updating to %s",
                     defs.READY_FOR_QA_LABEL, defs.READY_FOR_MERGE_LABEL, defs.BZ_STATE_MODIFIED)
        return

    # Do not update bugs when the MR is in Draft state
    if merge_request.draft:
        LOGGER.debug("Not updating bugs for MR %s, because it's marked as a Draft.",
                     merge_request.iid)
        return

    for bug_no in bug_list:
        # This isn't a bug number, we can't tell bugzilla to change it's state
        if bug_no == 'INTERNAL':
            LOGGER.debug("This isn't a bug, it's a commit or MR flagged as INTERNAL.")
            continue

        bug = bzcon.getbug(bug_no, include_fields=BUG_FIELDS)
        if not bug or bug.id != int(bug_no):
            LOGGER.error("getbug() did not return a useful result for BZ %s.", bug_no)
            continue

        # Do we even need to do anything for this bug?
        if bug.status == defs.BZ_STATE_MODIFIED:
            LOGGER.debug("Bug %s already in state %s, nothing to do here.", bug.id, bug.status)
            continue

        # Do not set bug to MODIFIED if FailedQA is present in Verified field
        if defs.BZ_FAILED_QA in bug.cf_verified and bug.status != defs.BZ_STATE_POST:
            LOGGER.warning("Bug %s has %s set on it, not updating state from %s to %s.",
                           bug_no, defs.BZ_FAILED_QA, bug.status, defs.BZ_STATE_MODIFIED)
            continue

        if bug.status == defs.BZ_STATE_POST:
            LOGGER.debug("Will update bug %s from state: %s.", bug_no, bug.status)
            update_bug_list.append(bug.id)
        else:
            LOGGER.debug("Not modifying bug %s from state: %s.", bug_no, bug.status)

    if update_bug_list:
        update = bzcon.build_update(status=defs.BZ_STATE_MODIFIED)
        bzcon.update_bugs(update_bug_list, update)


def run_bz_validation(instance, project, merge_request, force_show_summary):
    # pylint: disable=too-many-locals
    """Perform the Bugzilla validation for a merge request."""
    # If we do not connect to bugzilla then we cannot fully validate BZ readiness.
    bzcon = common.try_bugzilla_conn()
    if not bzcon:
        raise ConnectionError('Bugzilla API connection failed. Aborting.')
    bugzilla_ids = common.extract_bzs(merge_request.description)
    bz_dependencies = common.extract_dependencies(project, merge_request.description)
    LOGGER.info("Running BZ validation for merge request: iid=%s, description=%s, bz=%s, deps=%s",
                merge_request.iid, merge_request.description, bugzilla_ids, bz_dependencies)

    (notes, bz_target, table, approved) = check_on_bzs(project, merge_request,
                                                       bugzilla_ids,
                                                       bz_dependencies, bzcon)

    if approved is BZState.READY_FOR_MERGE:
        status = defs.READY_SUFFIX
    elif approved is BZState.READY_FOR_QA:
        status = defs.NEEDS_TESTING_SUFFIX
    else:
        status = defs.NEEDS_REVIEW_SUFFIX
    label_changed = common.add_label_to_merge_request(instance, project, merge_request.iid,
                                                      [f'Bugzilla::{status}'])
    if approved:
        set_bugs_to_modified(project, merge_request.iid, bugzilla_ids, bzcon)

    if not misc.is_production():
        LOGGER.info(print_text_report(merge_request, notes, bz_target, table, approved))

    if misc.is_production() and ((label_changed and not approved) or force_show_summary):
        report = print_gitlab_report(merge_request, notes, bz_target, table, approved)
        merge_request.notes.create({'body': report})

    cdlib.set_dependencies_label(instance, project, merge_request)


def bz_process_umb(headers, **_):
    """Process a message sent by the umb_bridge."""
    namespace = headers['mrpath'].split('!')[0]
    mr_id = headers['mrpath'].split('!')[-1]
    url = f'https://gitlab.com/{namespace}/-/merge_requests/{mr_id}'

    gl_instance, project, merge_request, *_ = common.parse_mr_url(url)
    run_bz_validation(gl_instance, project, merge_request, False)


def bz_process_mr(gl_instance, message, **_):
    """Process a merge request message."""
    project = gl_instance.projects.get(message.payload["project"]["id"])
    if not (merge_request := common.get_mr(project, message.payload["object_attributes"]["iid"])):
        return
    run_bz_validation(gl_instance, project, merge_request, False)


def bz_process_note(gl_instance, message, **_):
    """Process a note message."""
    LOGGER.debug("Checking note request\n")
    if "merge_request" not in message.payload:
        return

    force_eval = common.force_webhook_evaluation(message.payload['object_attributes']['note'], 'bz')
    if not force_eval:
        return

    project = gl_instance.projects.get(message.payload["project"]["id"])
    if not (merge_request := common.get_mr(project, message.payload["merge_request"]["iid"])):
        return
    run_bz_validation(gl_instance, project, merge_request, True)


WEBHOOKS = {
    "merge_request": bz_process_mr,
    "note": bz_process_note,
    defs.UMB_BRIDGE_MESSAGE_TYPE: bz_process_umb
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('BUGZILLA')
    args = parser.parse_args(args)
    common.generic_loop(args, WEBHOOKS)


if __name__ == "__main__":
    main(sys.argv[1:])
