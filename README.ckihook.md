# ckihook Webhook

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.ckihook \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \
	--action pipeline

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.
