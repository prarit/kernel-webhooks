# UMB Bridge Webhook (umb_bridge)

This is a special service which listens for and acts upon both Gitlab Webhook
events & events provided by the [amqp-bridge][1].

At startup, the umb_bridge builds a cache of all BZs currently referenced by
open MRs. During runtime it listens for MR events to keep the cache up to date.
When a BZ event is received from the amqp-bridge which is determined to be
relevant to an active MR the umb_bridge will send a message to the AMQP queue
for the [bugzilla][2] webhook to pick up.

The umb_bridge runs a separate thread to handle its send queue as defined by
its SendQueue class. When a bug is modified it may generate a flurry of
amqp-bridge events so the umb_bridge send_queue only holds one notice per MR
and only sends that notice after it has been in the queue for at least a
second.

Running the service requires the following parameters:

    python3 -m webhook.umb_bridge \
        --rabbitmq-host RABBITMQ_HOST \
        --rabbitmq-port RABBITMQ_PORT \
        --rabbitmq-user RABBITMQ_USER \
        --rabbitmq-password RABBITMQ_PASSWORD \
        --rabbitmq-exchange WEBHOOK_RECEIVER_EXCHANGE \
        --rabbitmq-routing-key UMB_BRIDGE_ROUTING_KEYS \
        --rabbitmq-sender-exchange UMB_BRIDGE_SENDER_EXCHANGE \
        --rabbitmq-sender-route UMB_BRIDGE_SENDER_ROUTE \
        --rabbitmq-queue-name UMB_BRIDGE_QUEUE (optional unless IS_PRODUCTION=true)

The user must have permissions to send messages to the
UMB_BRIDGE_SENDER_EXCHANGE exchange. UMB_BRIDGE_SENDER_ROUTE is the routing key
that will be used for the message.

The message sent will have the following simple header format. There is no body.

    {
      "message-type": <defs.UMB_BRIDGE_MESSAGE_TYPE>,
      "mrpath": <MR Reference in "full" format>
    }

ex:

    {
      "message-type": "cki.kwf.umb-bz-event",
      "mrpath": "redhat/rhel/kernel-test!123"
    }


[1]: https://gitlab.com/cki-project/cki-tools/-/blob/main/README.amqp_bridge.md
[2]: README.bugzilla.md
