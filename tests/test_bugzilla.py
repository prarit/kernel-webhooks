"""Webhook interaction tests."""
import copy
import json
import os
from unittest import TestCase
from unittest import mock

from cki_lib import session
from gitlab.exceptions import GitlabGetError

from webhook import bugzilla
from webhook import common
from webhook import defs


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestBugzilla(TestCase):
    """ Test Webhook class."""

    PAYLOAD_MERGE = {'object_kind': 'merge_request',
                     'project': {'id': 1,
                                 'web_url': 'https://web.url/g/p'},
                     'object_attributes': {'target_branch': 'main', 'iid': 2},
                     'state': 'opened'
                     }

    PAYLOAD_NOTE = {'object_kind': 'note',
                    'project': {'id': 1,
                                'web_url': 'https://web.url/g/p'},
                    'object_attributes': {'note': 'comment',
                                          'noteable_type': 'MergeRequest'},
                    'merge_request': {'target_branch': 'main', 'iid': 2},
                    'state': 'opened'
                    }

    BZ_RESULTS = [{'id': 1234567,
                   'external_bugs': [{'ext_bz_bug_id': '111222',
                                      'type': {'description': 'Linux Kernel',
                                               'url': 'https://bugzilla.kernel.org'
                                               }
                                      },
                                     {'ext_bz_bug_id': 'redhat/rhel/8.y/kernel/-/merge_requests/10',
                                      'type': {'description': 'Gitlab',
                                               'url': 'https://gitlab.com/'
                                               }
                                      }]
                   },
                  {'id': 8675309,
                   'external_bugs': [{'ext_bz_bug_id': '11223344',
                                      'type': {'description': 'Linux Kernel',
                                               'url': 'https://bugzilla.kernel.org'
                                               }
                                      },
                                     {'ext_bz_bug_id': 'group/project/-/merge_requests/321',
                                      'type': {'description': 'Gitlab',
                                               'url': 'https://gitlab.com/'
                                               }
                                      }]
                   },
                  {'id': 66442200,
                   'external_bugs': []
                   }]

    def test_get_project_file(self):
        project = mock.Mock()
        project.files.raw = mock.Mock(return_value='raw file')
        project.files.get = mock.Mock(return_value='file')

        # Success with raw.
        result = bugzilla.get_project_file(project, 'main', 'Makefile')
        self.assertEqual(result, 'raw file')
        project.files.get.assert_not_called()

        # Success without raw.
        project.files.raw.reset_mock()
        project.files.get.reset_mock()
        result = bugzilla.get_project_file(project, 'main', 'Makefile', raw=False)
        self.assertEqual(result, 'file')
        project.files.raw.assert_not_called()

        # Exception.
        with self.assertLogs('cki.webhook.bugzilla', level='INFO') as logs:
            project.files.raw.side_effect = GitlabGetError
            result = bugzilla.get_project_file(project, 'main', 'Makefile')
            self.assertEqual(result, None)
            self.assertIn('Failed to fetch.', ' '.join(logs.output))

    @mock.patch('webhook.bugzilla.get_project_file')
    def test_get_project_major_ver(self, mock_get_file):
        project = mock.Mock()

        # Project name is 'rhel-#' format or rhel-alt-#.
        project.name = 'rhel-7'
        result = bugzilla.get_project_major_ver(project)
        self.assertEqual(result, '7')
        project.name = 'rhel-10-alpha'
        result = bugzilla.get_project_major_ver(project)
        self.assertEqual(result, '10')
        project.name = 'rhel-alt-7'
        result = bugzilla.get_project_major_ver(project)
        self.assertEqual(result, '7')
        project.name = 'centos-stream-8'
        result = bugzilla.get_project_major_ver(project)
        self.assertEqual(result, '8')

        # Try and fail to get the makefile.
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            project.name = 'kernel-test'
            mock_get_file.return_value = None
            result = bugzilla.get_project_major_ver(project)
            self.assertEqual(result, None)
            self.assertIn(("Unable to determine major version of project"
                           " 'kernel-test' from its name."), logs.output[-1])
            self.assertNotIn('Cannot find RHEL_MAJOR in Makefile.', ' '.join(logs.output))
            self.assertEqual(mock_get_file.call_count, 2)

        # Get version from file.
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            project.name = 'kernel-test'
            mock_get_file.side_effect = [None, b'RHEL_MINOR = 2\nRHEL_MAJOR = 8\n']
            result = bugzilla.get_project_major_ver(project)
            self.assertEqual(result, '8')
            self.assertIn(("Unable to determine major version of project"
                           " 'kernel-test' from its name."), logs.output[-1])

        # Fail completely :(.
        with self.assertLogs('cki.webhook.bugzilla', level='ERROR') as logs:
            project.name = 'kernel-test'
            mock_get_file.side_effect = [None, b'RHEL_MINOR = 2\nRHEL_MEJOR = 8\n']
            result = bugzilla.get_project_major_ver(project)
            self.assertEqual(result, None)
            self.assertIn('Cannot find RHEL_MAJOR in Makefile.', logs.output[-1])

    @mock.patch('webhook.bugzilla.get_project_file')
    def test_get_mr_target_release(self, mock_get_file):
        project = mock.Mock()

        # Unable to determine major_ver, thus not able to get target
        project.name = 'kernel-test'
        mock_get_file.return_value = None
        result = bugzilla.get_mr_target_release(project, 'X.Y')
        self.assertEqual(result, None)

        # A centos-stream project MR.
        project.name = 'centos-stream-9'
        result = bugzilla.get_mr_target_release(project, 'main')
        self.assertEqual(result, 'c9s')

        # Target is not 'main', test zstream/alpha/beta branches.
        project.name = 'rhel-9'
        result = bugzilla.get_mr_target_release(project, '8.4')
        self.assertEqual(result, None)
        result = bugzilla.get_mr_target_release(project, '9.0-alpha')
        self.assertEqual(result, 'rhel-9.0.0-alpha')
        result = bugzilla.get_mr_target_release(project, '9.0-beta')
        self.assertEqual(result, 'rhel-9.0.0-beta')
        result = bugzilla.get_mr_target_release(project, '9.1-alpha')
        self.assertEqual(result, 'rhel-9.1.0')
        project.name = 'rhel-8'
        result = bugzilla.get_mr_target_release(project, '8.2')
        self.assertEqual(result, 'rhel-8.2.0')

        # Target is 'main' so return project.branches.list + 1.
        project.name = 'rhel-8'
        branch_list = []
        for branch in range(12):
            new_branch = mock.Mock()
            new_branch.name = f'8.{branch}'
            branch_list.append(new_branch)
        project.branches.list.return_value = branch_list
        result = bugzilla.get_mr_target_release(project, 'main')
        self.assertEqual(result, 'rhel-8.12.0')

        # The branch naming scheme is diferent before rhel8.
        project.name = 'rhel-7'
        branch_list = []
        for branch in range(9):
            new_branch = mock.Mock()
            new_branch.name = f'7.{branch}'
            branch_list.append(new_branch)
        project.branches.list.return_value = branch_list
        result = bugzilla.get_mr_target_release(project, 'main')
        self.assertEqual(result, 'rhel-7.9')
        project.name = 'rhel-7'
        result = bugzilla.get_mr_target_release(project, '7.9')
        self.assertEqual(result, 'rhel-7.9')
        result = bugzilla.get_mr_target_release(project, '7.3')
        self.assertEqual(result, 'rhel-7.3')
        project.name = 'rhel-6'
        result = bugzilla.get_mr_target_release(project, '6.10')
        self.assertEqual(result, 'rhel-6.10')
        project.name = 'rhel-alt-7'
        result = bugzilla.get_mr_target_release(project, 'main')
        self.assertEqual(result, 'rhel-alt-7.9')
        project.name = 'rhel-alt-7'
        result = bugzilla.get_mr_target_release(project, '7.4')
        self.assertEqual(result, 'rhel-alt-7.4')

    def test_bug_is_verified(self):
        bug_no = '12345678'
        bzcon = mock.Mock()
        bug_fields = bugzilla.BUG_FIELDS

        # getbug() failure.
        with self.assertLogs('cki.webhook.bugzilla', level='ERROR') as logs:
            bzcon.getbug.return_value = None
            self.assertFalse(bugzilla.bug_is_verified(bug_no, bzcon))
            bzcon.getbug.called_once_with(bug_no, bug_fields)
            self.assertIn('getbug() did not return a useful result for BZ 12345678.',
                          logs.output[-1])

        # getbug() returns an unverified bug.
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            bug = mock.Mock(id=12345678, product='Red Hat Enterprise Linux 8', component='kernel',
                            cf_verified=[])
            bzcon.getbug.return_value = bug
            self.assertFalse(bugzilla.bug_is_verified(bug_no, bzcon))
            bzcon.getbug.called_once_with(bug_no, bug_fields)
            self.assertIn('Bug 12345678 is not Verified.', logs.output[-1])

        # getbug() returns an verified bug for kernel.
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            bug = mock.Mock(id=12345678, product='Red Hat Enterprise Linux 8', component='kernel',
                            cf_verified=['Tested'])
            bzcon.getbug.return_value = bug
            self.assertTrue(bugzilla.bug_is_verified(bug_no, bzcon))
            bzcon.getbug.called_once_with(bug_no, bug_fields)
            self.assertIn('Bug 12345678 is Verified.', logs.output[-1])

        # getbug() returns an verified bug for kernel-rt.
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            bug = mock.Mock(id=12345678, product='Red Hat Enterprise Linux 8',
                            component='kernel-rt', cf_verified=['Tested'])
            bzcon.getbug.return_value = bug
            self.assertTrue(bugzilla.bug_is_verified(bug_no, bzcon))
            bzcon.getbug.called_once_with(bug_no, bug_fields)
            self.assertIn('Bug 12345678 is Verified.', logs.output[-1])

    def test_set_bugs_to_modified(self):
        bug1 = '12345678'
        bug1_data = mock.Mock(id=int(bug1), status='NEW', cf_verified='Verified')
        bug2 = '11223344'
        bug2_data = mock.Mock(id=int(bug2), status='ASSIGNED', cf_verified='Verified')
        bug3 = '24687531'
        bug3_data = mock.Mock(id=int(bug3), status='POST', cf_verified='Verified')
        bug4 = '8675309'
        bug4_data = mock.Mock(id=int(bug4), status='ON_QA', cf_verified='Verified')
        bug5 = '5551234'
        bug5_data = mock.Mock(id=int(bug5), status=defs.BZ_STATE_MODIFIED, cf_verified='Verified')
        bug6 = '5551234'
        bug6_data = mock.Mock(id=int(bug6), status='ASSIGNED', cf_verified=defs.BZ_FAILED_QA)
        bug7 = 'INTERNAL'
        bug7_data = mock.Mock(id=bug7, status='POST', cf_verified='Verified')
        project = mock.Mock()
        merge_request = mock.Mock()
        merge_request.draft = True
        merge_request.iid = 1234
        merge_request.labels = []
        project.mergerequests.get.return_value = merge_request
        bzcon = mock.Mock()
        bug_fields = bugzilla.BUG_FIELDS

        # MR does not have readyForQA label on it
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            bzcon.getbug.return_value = bug1_data
            bugzilla.set_bugs_to_modified(project, merge_request.iid, [bug1], bzcon)
            bzcon.getbug.called_once_with(bug1, bug_fields)
            self.assertIn(f'This merge request has no {defs.READY_FOR_QA_LABEL} or '
                          f'{defs.READY_FOR_MERGE_LABEL} label, not '
                          f'updating to {defs.BZ_STATE_MODIFIED}',
                          logs.output[-1])

        merge_request.labels = [defs.READY_FOR_MERGE_LABEL]

        # MR is in draft state and has readyForMerge label set, do nothing
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            bzcon.getbug.return_value = bug1_data
            bugzilla.set_bugs_to_modified(project, merge_request.iid, [bug1], bzcon)
            bzcon.getbug.called_once_with(bug1, bug_fields)
            self.assertIn('Not updating bugs for MR 1234, because it\'s marked as a Draft.',
                          logs.output[-1])

        merge_request.labels = [defs.READY_FOR_QA_LABEL]

        # MR is in draft state and has readyForQA label set, do nothing
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            bzcon.getbug.return_value = bug1_data
            bugzilla.set_bugs_to_modified(project, merge_request.iid, [bug1], bzcon)
            bzcon.getbug.called_once_with(bug1, bug_fields)
            self.assertIn('Not updating bugs for MR 1234, because it\'s marked as a Draft.',
                          logs.output[-1])

        merge_request.draft = False

        # getbug() failure.
        with self.assertLogs('cki.webhook.bugzilla', level='ERROR') as logs:
            bzcon.getbug.return_value = None
            bugzilla.set_bugs_to_modified(project, merge_request.iid, [bug1], bzcon)
            bzcon.getbug.called_once_with(bug1, bug_fields)
            self.assertIn('getbug() did not return a useful result for BZ 12345678.',
                          logs.output[-1])

        # Bug will not be updated from NEW to MODIFIED
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            bzcon.getbug.return_value = bug1_data
            bugzilla.set_bugs_to_modified(project, merge_request.iid, [bug1], bzcon)
            self.assertIn(f'Not modifying bug {bug1_data.id} from state: {bug1_data.status}.',
                          logs.output[-1])

        # Bug will not be updated from ASSIGNED to MODIFIED
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            bzcon.getbug.return_value = bug2_data
            bugzilla.set_bugs_to_modified(project, merge_request.iid, [bug2], bzcon)
            self.assertIn(f'Not modifying bug {bug2_data.id} from state: {bug2_data.status}.',
                          logs.output[-1])

        # Bug will be updated from POST to MODIFIED
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            bzcon.getbug.return_value = bug3_data
            bugzilla.set_bugs_to_modified(project, merge_request.iid, [bug3], bzcon)
            self.assertIn(f'Will update bug {bug3_data.id} from state: {bug3_data.status}.',
                          logs.output[-1])

        # Bug will not be updated from ON_QA
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            bzcon.getbug.return_value = bug4_data
            bugzilla.set_bugs_to_modified(project, merge_request.iid, [bug4], bzcon)
            self.assertIn(f'Not modifying bug {bug4_data.id} from state: {bug4_data.status}.',
                          logs.output[-1])

        # Bug will not be updated from MODIFIED
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            bzcon.getbug.return_value = bug5_data
            bugzilla.set_bugs_to_modified(project, merge_request.iid, [bug5], bzcon)
            self.assertIn(f'Bug {bug5_data.id} already in state {bug5_data.status}, nothing '
                          'to do here.',
                          logs.output[-1])

        # Bug will not be moved to MODIFIED if it has Verified: FailedQA and not in POST
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            bzcon.getbug.return_value = bug6_data
            bugzilla.set_bugs_to_modified(project, merge_request.iid, [bug6], bzcon)
            self.assertIn(f'Bug {bug6_data.id} has {defs.BZ_FAILED_QA} set on it, not updating '
                          f'state from {bug6_data.status} to {defs.BZ_STATE_MODIFIED}.',
                          logs.output[-1])

        # There's nothing to do for commits/MRs flagged as INTERNAL
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            bzcon.getbug.return_value = bug7_data
            bugzilla.set_bugs_to_modified(project, merge_request.iid, [bug7], bzcon)
            self.assertIn("This isn't a bug, it's a commit or MR flagged as INTERNAL.",
                          logs.output[-1])

    def test_temporary_xmlrpc_fault(self):
        # Not temporary
        test_str = ('*** Checking commit HEAD\n*** Resolves:\n***   Invalid:\n***     rhbz#20210325'
                    '(error: XMLRPC Fault #101 from Bugzilla: Bug #20210325 does not exist.)\n')
        result = bugzilla.temporary_xmlrpc_fault(test_str)
        self.assertFalse(result)

        # Temporary
        test_str = ('*** Checking commit HEAD\n*** Resolves:\n***   Invalid:\n***     rhbz#12345678'
                    '(error: <ProtocolError for bugzilla.redhat.com/xmlrpc.cgi: 503 Service '
                    'Unavailable>)\n')
        result = bugzilla.temporary_xmlrpc_fault(test_str)
        self.assertTrue(result)

    def test_validate_gitbz_response(self):
        mock_response = mock.Mock()

        # Bad status code.
        mock_response.status_code = 504
        output = bugzilla.validate_gitbz_response(mock_response)

        self.assertEqual(output, mock_response)
        self.assertEqual(output.status_code, 504)

        # Good response code, query returns 'ok'
        mock_response.status_code = 200
        mock_response.json.return_value = {'result': 'ok',
                                           'logs': 'bla bla bla'
                                           }
        output = bugzilla.validate_gitbz_response(mock_response)

        self.assertEqual(output, mock_response)
        self.assertEqual(output.result, 'ok')
        self.assertEqual(output.status_code, 200)

        # Good response code, query returns 'fail' and no error.
        mock_response.json.return_value = {'result': 'fail',
                                           'logs': 'bla bla bla'
                                           }
        output = bugzilla.validate_gitbz_response(mock_response)

        self.assertEqual(output, mock_response)
        self.assertEqual(output.result, 'fail')
        self.assertEqual(output.status_code, 200)

        # Good response code, query returns 'fail' but 'logs' show a problem.
        mock_response.json.return_value = {'result': 'fail',
                                           'logs': 'bla bla bla Invalid bla bla bla'
                                           }
        output = bugzilla.validate_gitbz_response(mock_response)

        self.assertEqual(output, mock_response)
        self.assertEqual(output.result, 'fail')
        self.assertEqual(output.status_code, 502)

    # pylint: disable=no-self-use
    @mock.patch('cki_lib.messagequeue.pika.BlockingConnection')
    @mock.patch('webhook.common.process_message')
    @mock.patch.dict(os.environ, {'BUGZILLA_ROUTING_KEYS': 'mocked', 'BUGZILLA_QUEUE': 'mocked'})
    def test_entrypoint(self, mocked_process, mocked_connection):
        """Check basic queue consumption."""

        # setup dummy consume data
        payload = {'foo': 'bar'}
        properties = mock.Mock()
        headers = {'header-key': 'value'}
        setattr(properties, 'headers', headers)
        consume_value = [(mock.Mock(), properties, json.dumps(payload))]
        mocked_connection().channel().consume.return_value = consume_value

        bugzilla.main([])

        mocked_process.assert_called()

    @mock.patch('gitlab.Gitlab')
    def test_unsupported_object_kind(self, mocked_gitlab):
        """Check handling an unsupported object kind."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload.update({'object_kind': 'foo'})

        self._test_payload(mocked_gitlab, False, payload, False)

    def test_validate_internal_bz(self):
        """Check for expected return value."""
        commits = {"deadb": ['redhat/Makefile']}
        self.assertTrue(bugzilla.validate_internal_bz(commits))
        commits = {"deadb": ['redhat/Makefile', '.gitlab-ci.yml']}
        self.assertTrue(bugzilla.validate_internal_bz(commits))
        commits = {"deadb": ['.gitlab/CODEOWNERS', '.gitlab-ci.yml']}
        self.assertTrue(bugzilla.validate_internal_bz(commits))
        commits['abcde'] = ['net/core/dev.c', 'include/net/netdevice.h']
        self.assertFalse(bugzilla.validate_internal_bz(commits))
        commits['abcde'] = ['net/core/dev.c', 'include/net/netdevice.h', 'redhat/configs/']
        self.assertFalse(bugzilla.validate_internal_bz(commits))

    def test_build_review_lists(self):
        """Test out the build_review_lists function."""
        commit = mock.Mock(id="123456abcdef", message="This is a commit message w/no BZ\n")
        commit.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        review_lists = []
        bug = "1987253"
        output = bugzilla.build_review_lists(commit, review_lists, bug)
        self.assertEqual(output, {'commits': {'123456abcdef': []}})
        output = bugzilla.build_review_lists(None, review_lists, bug)
        self.assertEqual(output, {'commits': {defs.BZ_IN_DESCRIPTION_ONLY: []}})

    @mock.patch('webhook.bugzilla.get_mr_target_release')
    def test_print_text_report(self, mock_target):
        """Test for various bug issue reporting scenarios."""
        mreq = mock.Mock()
        mreq.target_branch = 'main'
        mock_target.return_value = mreq.target_branch
        table = []
        approval = False
        note_string = "This report is an absolute disaster."
        notes = [note_string]
        output = bugzilla.print_text_report(mreq, notes, mock_target, table, approval)
        self.assertIn(note_string, output)
        self.assertIn("There were no valid bugs in the MR description or in any commits", output)
        table = [['abcd', [defs.BZ_IN_DESCRIPTION_ONLY], 1, ['xyz']]]
        output = bugzilla.print_text_report(mreq, notes, mock_target, table, approval)
        self.assertIn("There are bugs in the MR description not in any commits", output)
        self.assertIn("Merge Request fails bugzilla validation", output)
        approval = True
        table = [['abcd', ['1234'], 1, ['xyz']]]
        output = bugzilla.print_text_report(mreq, notes, mock_target, table, approval)
        self.assertIn("Merge Request passes bugzilla validation", output)

    def test_get_report_table(self):
        """Ensure the value of the returned mr_approved is correct."""
        bug1 = {'validation': {'approved': bugzilla.BZState.READY_FOR_QA, 'notes': '', 'logs': ''},
                'commits': []}
        bug2 = {'validation': {'approved': bugzilla.BZState.READY_FOR_MERGE, 'notes': '',
                               'logs': ''},
                'commits': []}
        bug3 = {'validation': {'approved': bugzilla.BZState.NOT_READY, 'notes': '', 'logs': ''},
                'commits': []}

        # mr_approved should reflect lowest BZState value in the reviewed_items list.
        reviewed_items = {1234567: bug1, 2345678: bug2}
        (table, result) = bugzilla.get_report_table(reviewed_items)
        self.assertEqual(result, bugzilla.BZState.READY_FOR_QA)
        reviewed_items = {1234567: bug1, 2345678: bug2, 3456753: bug3}
        (table, result) = bugzilla.get_report_table(reviewed_items)
        self.assertEqual(result, bugzilla.BZState.NOT_READY)
        reviewed_items = {1234567: bug2, 2345678: bug2, 3456753: bug2}
        (table, result) = bugzilla.get_report_table(reviewed_items)
        self.assertEqual(result, bugzilla.BZState.READY_FOR_MERGE)

    @mock.patch('webhook.bugzilla.get_mr_target_release')
    def test_print_gitlab_report(self, mock_target):
        """Test for various bug issue reporting scenarios."""
        mreq = mock.Mock()
        mreq.target_branch = 'main'
        mock_target.return_value = mreq.target_branch
        table = []
        approval = False
        note_string = "This report is an absolute disaster."
        notes = [note_string]
        output = bugzilla.print_gitlab_report(mreq, notes, mock_target, table, approval)
        self.assertIn(note_string, output)
        self.assertIn("**Bugzilla Readiness Error(s)!**", output)
        self.assertIn("There were no valid bugs in the MR description or in any commits", output)
        table = [['abcd', [defs.BZ_IN_DESCRIPTION_ONLY], 1, ['xyz']]]
        output = bugzilla.print_gitlab_report(mreq, notes, mock_target, table, approval)
        self.assertIn("**Bugzilla Readiness Error(s)!**", output)
        self.assertIn("There are bugs in the MR description not in any commits", output)
        self.assertIn("Merge Request fails bugzilla validation", output)
        approval = True
        table = [['abcd', ['1234'], 1, ['xyz']]]
        output = bugzilla.print_gitlab_report(mreq, notes, mock_target, table, approval)
        self.assertIn("**Bugzilla Readiness Report**", output)
        self.assertIn("Merge Request passes bugzilla validation", output)

    @mock.patch('gitlab.Gitlab')
    def test_merge_request(self, mocked_gitlab):
        """Check handling of a merge request."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload['object_attributes']['target_branch'] = '8.3-net'
        self._test_payload(mocked_gitlab, True, payload, True)

    @mock.patch('gitlab.Gitlab')
    def test_note(self, mocked_gitlab):
        """Check handling of a note."""
        self._test_payload(mocked_gitlab, True, self.PAYLOAD_NOTE, False)

    @mock.patch('gitlab.Gitlab')
    def test_bz_re_evaluation1(self, mocked_gitlab):
        """Check handling of bz re-evaluation."""
        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = "request-evaluation"
        self._test_payload(mocked_gitlab, True, payload, True)

    @mock.patch('gitlab.Gitlab')
    def test_bz_re_evaluation2(self, mocked_gitlab):
        """Check handling of bz re-evaluation."""
        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = "request-bz-evaluation"
        self._test_payload(mocked_gitlab, True, payload, True)

    def _test_payload(self, mocked_gitlab, result, payload, assert_gitlab_called):
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            self._test(mocked_gitlab, result, payload, assert_gitlab_called)
        with mock.patch('cki_lib.misc.is_production', return_value=False):
            self._test(mocked_gitlab, result, payload, assert_gitlab_called)

    # pylint: disable=too-many-arguments,too-many-locals
    @mock.patch('webhook.common.try_bugzilla_conn')
    def _test(self, mocked_gitlab, result, payload, assert_gitlab_called, mock_try_bugzilla):

        # setup dummy gitlab data
        instance = mock.Mock()
        rh_group = mock.Mock(id=10810393)
        instance.groups.list.return_value = [rh_group]
        project = mock.Mock(name_with_namespace="foo.bar",
                            namespace={'name': '8.y'})
        project.name = 'rhel-8'
        project.namespace = mock.MagicMock(id=1, full_path="redhat/rhel/src/kernel")
        target = "main"
        if payload['object_kind'] == 'merge_request':
            target = payload['object_attributes']['target_branch']
        if payload['object_kind'] == 'note':
            target = payload['merge_request']['target_branch']
        merge_request = mock.MagicMock(target_branch=target)
        c1 = mock.Mock(id="1234",
                       message="1\n"
                       "Bugzilla: http://bugzilla.redhat.com/show_bug.cgi?id=1234",
                       parent_ids=["abcd"])
        c2 = mock.Mock(id="4567",
                       message="2\nBugzilla: http://bugzilla.test?id=45678\n"
                       "Bugzilla: https://bugzilla.redhat.com/4567\n"
                       "Depends: http://bugzilla.redhat.com/1234",
                       parent_ids=["1234"])
        c3 = mock.Mock(id="890a",
                       message="3\nBugzilla: INTERNAL",
                       parent_ids=["face"])
        c3.diff = mock.Mock(return_value=[{'new_path': 'redhat/Makefile'}])
        c4 = mock.Mock(id="face",
                       message="This is a merge",
                       parent_ids=["abcd", "4567"])
        c5 = mock.Mock(id="deadbeef",
                       message="5\nBugzilla: 565472, 7784378\n"
                       "Nothing to see here\n"
                       "Bugzilla: http://bugzilla.redhat.com/1234567\n"
                       "Bugzilla: https://bugzilla.redhat.com/7654321\n"
                       "Bugzilla: http://bugzilla.redhat.com/1989898",
                       parent_ids=["890a"])
        c6 = mock.Mock(id="1800555",
                       message="6\nBugzilla: http://bugzilla.redhat.com/show_bug.cgi?id=1800555",
                       parent_ids=["deadbeef"])
        c7 = mock.Mock(id="e404",
                       message="7\n"
                       "https://bugzilla.redhat.com/show_bug.cgi?id=9983023",
                       parent_ids=["1800555"])
        c8 = mock.Mock(id="abcd",
                       message="8\n"
                       "Depends: "
                       "https://bugzilla.redhat.com/show_bug.cgi?id=7654321\n",
                       parent_ids=["1800555"])
        mocked_gitlab().__enter__().projects.get.return_value = project
        project.mergerequests.get.return_value = merge_request
        project.labels.list.return_value = []
        merge_request.iid = 2
        merge_request.labels = []
        merge_request.commits.return_value = [c1, c2, c3, c4, c5, c6, c7, c8]
        merge_request.pipeline = {'status': 'success'}
        branch = mock.Mock()
        branch.configure_mock(name="8.2")
        project.branches.list.return_value = [branch]
        project.commits = {"1234": c1, "4567": c2, "890a": c3, "face": c4,
                           "deadbeef": c5, "1800555": c6, "e404": c7,
                           "abcd": c8}
        self.assertEqual(common.extract_bzs(c1.message), ["1234"])
        self.assertEqual(common.extract_dependencies(project, c1.message), [])
        self.assertEqual(common.extract_bzs(c2.message), ["4567"])
        self.assertEqual(common.extract_dependencies(project, c2.message),
                         ["1234"])
        self.assertEqual(common.extract_bzs(c3.message), ["INTERNAL"])
        self.assertEqual(common.extract_bzs(c5.message),
                         ["1234567", "7654321", "1989898"])
        self.assertEqual(common.extract_bzs(c6.message), ["1800555"])
        self.assertEqual(common.extract_bzs(c7.message), [])
        self.assertEqual(common.extract_dependencies(project, c8.message),
                         ["7654321"])

        # setup dummy post results data
        presult = mock.Mock()
        presult.json.return_value = {
            "error": "*** No approved issue IDs referenced in log message or"
                     " changelog for HEAD\n*** Unapproved issue IDs referenced"
                     " in log message or changelog for HEAD\n*** Commit HEAD"
                     " denied\n*** Current checkin policy requires:\n"
                     "    release == +\n"
                     "*** See https://rules.doc/doc for more information",
            "result": "fail",
            "logs": "*** Checking commit HEAD\n*** Resolves:\n***   "
                    "Unapproved:\n***     rhbz#1234 (release?, qa_ack?, "
                    "devel_ack?, mirror+)\n"
        }

        with mock.patch('cki_lib.session.get_session') as mock_session:
            mock_session.side_effect = session.get_session()
            mock_session.post.return_value = presult
            headers = {'message-type': 'gitlab'}
            with mock.patch('webhook.bugzilla.get_mr_target_release', return_value='rhel-8.2.0'):
                return_value = \
                    common.process_message(bugzilla.WEBHOOKS, 'dummy', payload, headers,
                                           ignore_msgs_from_self=False)

        if result:
            mocked_gitlab.assert_called_with(
                'https://web.url', private_token='TOKEN',
                session=mock.ANY)

            self.assertTrue(return_value)
            if assert_gitlab_called:
                mocked_gitlab().__enter__().projects.get.assert_called_with(1)
                project.mergerequests.get.assert_called_with(2)
            else:
                mocked_gitlab().__enter__().projects.get.assert_not_called()
                project.mergerequests.get.assert_not_called()
        else:
            self.assertFalse(return_value)

    @mock.patch('webhook.common.parse_mr_url')
    @mock.patch('webhook.bugzilla.run_bz_validation')
    def test_bz_process_umb(self, mock_run_bz, mock_parse):
        url = 'https://gitlab.com/group/project/-/merge_requests/123'
        headers = {'message-type': defs.UMB_BRIDGE_MESSAGE_TYPE,
                   'mrpath': 'group/project!123'
                   }

        mock_parse.return_value = ('instance', 'project', 'merge_request')

        bugzilla.bz_process_umb(headers)
        mock_parse.assert_called_with(url)
        mock_run_bz.assert_called_with('instance', 'project', 'merge_request', False)
