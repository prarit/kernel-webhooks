# Bugzilla Webhook

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.bugzilla \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \
        --note request-bz-evaluation

The --note parameter "fakes" a comment being added to a merge request, as if
you added it through the gitlab webui yourself (but the comment itself isn't
actually added there). The example above shows the predefined comment keyword we
have to trigger bugzilla re-evaluation.

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

In addition, this webhook requires a bugzilla API key. Generate one from
[here](https://bugzilla.redhat.com/userprefs.cgi?tab=apikey) and then set
the corresponding environment variable `export BUGZILLA_API_KEY='1234567890abcedf'`.
